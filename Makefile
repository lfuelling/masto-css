SASS = sassc

all: $(patsubst %.scss,%.user.css,$(notdir $(wildcard src/*.scss)))

clean:
	rm -f *.user.css

%.user.css: src/%.scss
	$(SASS) src/$*.scss $@
