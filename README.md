# Mastodon CSS… stuff

Attempts to address gripes with Mastodon using only CSS, the most passive-aggressive language.

These are all compatible with [Dizzydon](https://github.com/dizzy-labs/dizzydon), btw.
It disables the stretchy columns but you probably wanted it to.

## Anti-Clickjacking

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/feed-noclick.user.css)

Accessibility aid that disables clicking buttons on posts unless you hover over them for a bit.
Solves the *very* common complaint of misaimed clicks caused by the feed updating under the pointer.

## Dense Column Layout

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/dense-columns.user.css)

Reformats the UI so you can get more good stuff on the screen:

* Columns stretch to the screen width.
  If you have more than will reasonably fit, it'll scroll as usual.
* On sufficiently tall screens you get an extra column space under the message box.
  You can stick your notifications there, Pleroma style.

## Vague Mode

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/numberless.user.css)

Adds options to remove distracting numbers (post/user stats).

## Miscellaneous Fixes

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/bugfixes.user.css)

I'll add them as I find them.

## Dense Post Layout (WIP)

[![Install directly with Stylus][install-button]](https://gitlab.com/flussence/masto-css/raw/stable/dense-posts.user.css)

Messes up the posts significantly for dubious gains.
The settings are mislabeled and don't work right yet, please ignore them.

* Removes the left margin and moves avatars to the right to wrap text around
* Long paragraphs take up slightly less vertical space
* Image thumbnails are slightly larger and take up slightly more vertical space

# Hacking

Edit `.scss` files. Run `make`. Have `sassc` from https://github.com/sass/libsass installed.

# License

All files here are under the GNU Affero General Public License 3.0,
go ahead and steal bits of them for your own instance if you want.

[install-button]: https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg
